# Recipe app API Proxy

NGINX proxy app for our receip pp API


## Usage
### Envinronment Variables

* `LISTEN_PORT` - Port on which this app is listening (default: `8000`)
* `APP_HOST` - hostname of the app for which request is forwarded (default: `app`)
* `APP_PORT` - Port of the app to forward request (default: `9000`)